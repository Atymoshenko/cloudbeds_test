<?php

/** @TODO: Sorry for anti pattern below. */
require __DIR__ . '/../src/NotFoundException.php';
require __DIR__ . '/../src/UserException.php';
require __DIR__ . '/../src/Api.php';
require __DIR__ . '/../src/CrudController.php';
require __DIR__ . '/../src/Db.php';
require __DIR__ . '/../src/App.php';
require __DIR__ . '/../src/Request.php';
require __DIR__ . '/../src/IntervalManager.php';

$app = new App();
$app->run();
