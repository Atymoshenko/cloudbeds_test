$(function() {
	'use strict';

	var $formIntervalCreate = $('form[name="interval-create"]');
	var $buttonClearAll = $('[data-clear="all"]');
	var $buttonDelete = $('a[data-delete]');
	$formIntervalCreate.on('submit', function(e) {
		createInterval($(this), e);
	});
	$buttonClearAll.on('click', function(e) {
		clearAll($(this), e);
	});
	$buttonDelete.on('click', function(e) {
		deleteInterval($(this), e);
	});
	function createInterval($form, e) {
		e.preventDefault();
		if (isNaN(createInterval.inProcess)) createInterval.inProcess = false;
		if (createInterval.inProcess) return;
		createInterval.inProcess = true;
		var formData = $form.serializeArray();
		$.ajax({
			url: $form.attr('action'),
			method: $form.attr('method'),
			data: formData,
			complete: function() {
				createInterval.inProcess = false;
			}
			, success: function(data) {
				if (data.success === true) {
					document.location.reload(true);
				}
			}
			, error: function(data) {
				window.alert(data.responseJSON.message);
			}
		});
	}
	function deleteInterval($button, e) {
		e.preventDefault();
		if (isNaN(deleteInterval.inProcess)) deleteInterval.inProcess = false;
		if (deleteInterval.inProcess) return;
		deleteInterval.inProcess = true;
		$.ajax({
			url: $button.attr('href'),
			method: 'DELETE',
			complete: function() {
				deleteInterval.inProcess = false;
			}
			, success: function(data) {
				if (data.success === true) {
					document.location.reload(true);
				}
			}
			, error: function(data) {
				window.alert(data.responseJSON.message);
			}
		});
	}
	function clearAll($button, e) {
		e.preventDefault();
		if (isNaN(clearAll.inProcess)) clearAll.inProcess = false;
		if (clearAll.inProcess) return;
		clearAll.inProcess = true;
		$.ajax({
			url: $button.data('url'),
			method: 'DELETE',
			complete: function() {
				clearAll.inProcess = false;
			}
			, success: function(data) {
				if (data.success === true) {
					document.location.reload(true);
				}
			}
			, error: function(data) {
				window.alert(data.responseJSON.message);
			}
		});
	}
});
