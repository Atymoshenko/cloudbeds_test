CREATE DATABASE `cloudbeds` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cloudbeds`;
CREATE TABLE `interval` (
    `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `date_start` date NOT NULL,
    `date_end` date NOT NULL,
    `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `interval` ADD UNIQUE `date_start_date_end` (`date_start`, `date_end`);
