<?php
/**
 * @var string $actionView
 */
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Cloudbeds test</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/common.css">
</head>
<body>
<div class="container mt-5">
	<?php require_once $actionView; ?>
</div>
<script src="jQuery/jquery-3.3.1.min.js"></script>
<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="js/common.js"></script>
</body>
</html>