<?php
/**
 * @var $intervalList[]
 */
?>
<div>
	<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">Create new interval</button>
	<button type="button" class="btn btn-danger float-left" data-clear="all" data-url="?action=api&method=interval/clear/all">Clear all intervals</button>
</div>

<div class="mt-3">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Id</th>
				<th>Interval</th>
				<th>Price</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($intervalList as $interval): ?>
			<tr>
				<td><?php echo $interval['id']; ?></td>
				<?php
				$dateStart = DateTime::createFromFormat('Y-m-d', $interval['date_start']);
				$dateEnd = DateTime::createFromFormat('Y-m-d', $interval['date_end']);
				$dateIntervalString = $dateStart->format('d.m');
				if ($dateStart != $dateEnd) {
					$dateIntervalString.= '-' . $dateEnd->format('d.m');
				}
				?>
				<td><?php echo $dateIntervalString; ?></td>
				<td><?php echo $interval['price']; ?></td>
				<td>
					<a href="?action=api&method=interval/delete&id=<?php echo $interval['id']; ?>" target="_self" data-delete>Delete</a>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" action="?action=api&method=interval/create" name="interval-create">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="date_start" class="required">Date start</label>
					<input type="date" name="date_start" class="form-control" id="date_start" required>
				</div>
				<div class="form-group">
					<label for="date_end" class="required">Date end</label>
					<input type="date" name="date_end" class="form-control" id="date_end" required>
				</div>
				<div class="form-group">
					<label for="price" class="required">Price</label>
					<input type="text" name="price" class="form-control" id="price" required>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-primary">Create</button>
			</div>
			</form>
		</div>
	</div>
</div>