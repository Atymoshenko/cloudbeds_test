<?php

class Request {
	private const DATE_PATTERN = "~^\d{4}-\d{2}-\d{2}$~";
	private const PRICE_PATTERN = "~^\d{1,9}((\.|,)\d{1,2})?$~";

	/** @var array */
	private $request;

	public function __construct(array $request)
	{
		$this->request = $request;
	}

	public function getRequest(): array
	{
		return $this->request;
	}

	/**
	 * @throws UserException
	 */
	public function validate(): void
	{
		if (!isset($this->getRequest()['date_start'])) {
			throw new UserException("Date start is required.");
		}
		if (!preg_match(self::DATE_PATTERN, $this->getRequest()['date_start'])) {
			throw new UserException("Invalid date start format, should be yyyy-mm-dd .");
		}

		if (!isset($this->getRequest()['date_end'])) {
			throw new UserException("Date end is required.");
		}
		if (!preg_match(self::DATE_PATTERN, $this->getRequest()['date_end'])) {
			throw new UserException("Invalid date end format, should be yyyy-mm-dd .");
		}

		if (!isset($this->getRequest()['price'])) {
			throw new UserException("Price is required.");
		}
		if (!preg_match(self::PRICE_PATTERN, $this->getRequest()['price'])) {
			throw new UserException("Invalid price format, should be 1-9 digits and mandatory dot and 1-2 digit(s).");
		}

		/**
		 * Code below for without framework.
		 */
		$dateStart = DateTime::createFromFormat('Y-m-d', $this->getRequest()['date_start']);
		$dateEnd = DateTime::createFromFormat('Y-m-d', $this->getRequest()['date_end']);
		if ($dateStart > $dateEnd) {
			$this->request['date_start'] = $dateEnd->format('Y-m-d');
			$this->request['date_end'] = $dateStart->format('Y-m-d');
		}
		$this->request['price'] = str_replace(',', '.', $this->getRequest()['price']);
	}
}