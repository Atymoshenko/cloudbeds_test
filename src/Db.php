<?php

class Db {
	/** @var string */
	private $host;
	/** @var string */
	private $name;
	/** @var string */
	private $user;
	/** @var string */
	private $password;
	/** @var mysqli */
	private $link;

	public function __construct(array $config)
	{
		$this->host = $config['db_host'];
		$this->user = $config['db_user'];
		$this->password = $config['db_password'];
		$this->name = $config['db_name'];
	}

	/**
	 * @return array
	 * @throws Exception
	 */
	public function getIntervals(): array
	{
		$sql = "SELECT `id`,`date_start`,`date_end`,`price` FROM `interval` ORDER BY `date_start`";
		$r = $this->query($sql);

		$result = [];
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $row;
		}

		return $result;
	}

	/**
	 * @param integer $id
	 *
	 * @return array
	 * @throws Exception
	 */
	public function getIntervalById(int $id): ?array
	{
		$sql = "SELECT `id`,`date_start`,`date_end`,`price` FROM `interval` WHERE `id`={$this->quote($id)}";
		$r = $this->query($sql);

		return $this->fetch_assoc($r);
	}

	/**
	 * @param string $dateStart
	 * @param string $dateEnd
	 *
	 * @return array
	 * @throws Exception
	 */
	public function getIntervalsByFilter(string $dateStart, string $dateEnd): array
	{
		$sql = "SELECT `id`,`date_start`,`date_end`,`price` FROM `interval`";
		$sql.= " WHERE `date_start` BETWEEN {$this->quote($dateStart)} AND {$this->quote($dateEnd)}";
		$sql.= " OR `date_end` BETWEEN {$this->quote($dateStart)} AND {$this->quote($dateEnd)}";
		$sql.= " OR (`date_start`<={$this->quote($dateEnd)} AND `date_end`>={$this->quote($dateStart)})";
		$sql.= " ORDER BY `date_start` ASC";
		$r = $this->query($sql);

		$result = [];
		while ($row = $this->fetch_assoc($r)) {
			$result[] = $row;
		}

		return $result;
	}

	/**
	 * @param int    $id
	 * @param string $dateStart
	 * @param string $dateEnd
	 * @param        $price
	 *
	 * @throws Exception
	 */
	public function saveInterval(int $id, string $dateStart, string $dateEnd, $price): void
	{
		$sql = "UPDATE `interval` SET `date_start`={$this->quote($dateStart)}, `date_end`={$this->quote($dateEnd)}, `price`={$this->quote($price)}  WHERE `id`={$this->quote($id)}";
		$this->query($sql);
	}

	/**
	 * @throws Exception
	 */
	public function removeAll(): void
	{
		$sql = "DELETE FROM `interval` WHERE 1";
		$this->query($sql);
	}

	/**
	 * @param mixed $criteria
	 *
	 * @throws Exception
	 */
	public function remove($criteria): void
	{
		if (is_array($criteria)) {
			$sql = "DELETE FROM `interval` WHERE `id` IN({$this->quote($criteria)})";
		} else {
			$sql = "DELETE FROM `interval` WHERE `id`={$this->quote($criteria)}";
		}
		$this->query($sql);
	}

	/**
	 * @param string $dateStart
	 * @param string $dateEnd
	 * @param float  $price
	 *
	 * @throws Exception
	 */
	public function createInterval(string $dateStart, string $dateEnd, $price): void
	{
		$sql = "INSERT INTO `interval` (`date_start`,`date_end`,`price`) VALUES  ({$this->quote($dateStart)}, {$this->quote($dateEnd)}, {$this->quote($price)})";
		$this->query($sql);
	}

	/**
	 * @param string $sql
	 *
	 * @return bool|mysqli_result
	 * @throws Exception
	 */
	private function query(string $sql)
	{
		$this->connect();
		$r = mysqli_query($this->link, $sql);
		if (!$r) {
			throw new Exception(mysqli_error($this->link) . " SQL: " . $sql);
		}

		return $r;
	}

	private function fetch_assoc(mysqli_result $r):? array
	{
		return mysqli_fetch_assoc($r);
	}

	/**
	 * @param mixed $data
	 *
	 * @return string
	 * @throws Exception
	 */
	private function quote($data): string
	{
		$this->connect();

		$type = gettype($data);

		switch ($type) {
			case 'string':
			case 'double':
				$data = "'" . mysqli_real_escape_string($this->link, $data) . "'";
				break;
			case 'integer':
				$data = (string)$data;
				break;
			case 'array':
				foreach ($data as $key => $value) {
					$subType = gettype($value);
					switch ($subType) {
						case 'string':
						case 'integer':
							$data[$key] = $this->quote($value);
						break;
						default:
							throw new Exception("Every value must be type string or integer. {$subType} given.");
					}
				}
				$data = implode(',', $data);
				break;
			default:
				throw new Exception("Undefined type {$type}.");
		}

		return $data;
	}

	/**
	 * @throws Exception
	 */
	private function connect(): void
	{
		if ($this->link) {
			return;
		}

		$this->link = @mysqli_connect($this->host, $this->user, $this->password, $this->name);
		if (mysqli_connect_error()) {
			throw new Exception('DB connect error');
		}
	}
}