<?php

class IntervalManager {
	/** @var Db */
	private $db;

	public function __construct(Db $db)
	{
		$this->db = $db;
	}

	private function getDb(): Db
	{
		return $this->db;
	}

	/**
	 * @param array    $existsIntervals
	 * @param DateTime $dateStart
	 * @param DateTime $dateEnd
	 * @param          $price
	 *
	 * @throws Exception
	 */
	public function createInterval(array $existsIntervals, DateTime $dateStart, DateTime $dateEnd, string $price): void
	{
		$freeAdd = true;
		$intervalIdsForDelete = [];
		foreach ($existsIntervals as $existsInterval) {
			$existsIntervalDateStart = DateTime::createFromFormat('Y-m-d', $existsInterval['date_start']);
			$existsIntervalDateEnd = DateTime::createFromFormat('Y-m-d', $existsInterval['date_end']);

			if ($existsIntervalDateStart >= $dateStart && $existsIntervalDateEnd <= $dateEnd) {
				$intervalIdsForDelete[] = $existsInterval['id'];
				continue;
			}

			if ($price != $existsInterval['price']) {
				$newDateStart = clone $existsIntervalDateStart;
				$newDateEnd = clone $existsIntervalDateEnd;
				if ($existsIntervalDateStart < $dateStart && $existsIntervalDateEnd > $dateEnd) {
					$newDateEnd = clone $dateStart;
					$newDateEnd->sub(new DateInterval('P1D'));
					$this->getDb()->saveInterval($existsInterval['id'], $existsIntervalDateStart->format('Y-m-d'), $newDateEnd->format('Y-m-d'), $existsInterval['price']);

					$newDateStart = clone $dateEnd;
					$newDateStart->add(new DateInterval('P1D'));
					$this->getDb()->createInterval($newDateStart->format('Y-m-d'), $existsIntervalDateEnd->format('Y-m-d'), $existsInterval['price']);
					continue;
				}
				if ($existsIntervalDateStart <= $dateEnd && $existsIntervalDateStart >= $dateStart) {
					$newDateStart = clone $dateEnd;
					$newDateStart->add(new DateInterval('P1D'));
				}
				if ($existsIntervalDateEnd >= $dateStart && $existsIntervalDateEnd <= $dateEnd) {
					$newDateEnd = clone $dateStart;
					$newDateEnd->sub(new DateInterval('P1D'));
				}
				$this->getDb()->saveInterval($existsInterval['id'], $newDateStart->format('Y-m-d'), $newDateEnd->format('Y-m-d'), $existsInterval['price']);
			} else {
				$freeAdd = false;
				$newDateStart = clone $existsIntervalDateStart;
				$newDateEnd = clone $existsIntervalDateEnd;
				if ($existsIntervalDateStart > $dateStart) {
					$newDateStart = clone $dateStart;
				}
				if ($existsIntervalDateEnd < $dateEnd) {
					$newDateEnd = clone $dateEnd;
				}
				$this->getDb()->saveInterval($existsInterval['id'], $newDateStart->format('Y-m-d'), $newDateEnd->format('Y-m-d'), $existsInterval['price']);
			}
		}

		if (count($intervalIdsForDelete)) {
			$this->getDb()->remove($intervalIdsForDelete);
		}
		if ($freeAdd) {
			$this->getDb()->createInterval($dateStart->format('Y-m-d'), $dateEnd->format('Y-m-d'), $price);
		}
	}
}