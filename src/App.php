<?php

/**
 * Class App
 * @author <servirus@gmail.com> Alexey Tymoshenko
 *
 *         This application not used framework.
 *         Common focus for demonstration for test Issue for Backend Senior Developer.
 *
 *         As for this application need PHP 7.1, i not write annotation for getters and setters.
 */
class App {
	/** @var Db */
	private $db;

	/**
	 * App constructor.
	 * Initialize properties.
	 */
	public function __construct()
	{
		$this->db = new Db(require_once __DIR__ . '/config.php');
	}

	public function getDb(): Db
	{
		return $this->db;
	}

	/**
	 * @throws Exception
	 */
	public function run(): void
	{
		new CrudController($this->getDb());
	}
}