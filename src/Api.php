<?php

/**
 * Class Api
 * Without framework methods call.
 */
class Api
{
	private const ACTION_INTERVAL_CREATE = 'interval/create';
	private const ACTION_INTERVAL_CLEAR_ALL = 'interval/clear/all';
	private const ACTION_INTERVAL_DELETE = 'interval/delete';

	/** @var string */
	private $method;
	/** @var Db */
	private $db;
	/** @var IntervalManager */
	private $intervalManager;
	/** @var array */
	private $statusTextByCode = [200 => 'OK', 404 => 'Not Found', 500 => 'Internal Server Error'];

	/**
	 * Api constructor.
	 *
	 * @param Db $db
	 */
	public function __construct(Db $db)
	{
		$this->db = $db;
		$this->intervalManager = new IntervalManager($this->getDb());
		$this->method = $_GET['method'] ?? null;
	}

	private function getDb(): Db
	{
		return $this->db;
	}

	public function getIntervalManager(): IntervalManager
	{
		return $this->intervalManager;
	}

	public function run(): void
	{
		try {
			switch ($this->getMethod()) {
				case self::ACTION_INTERVAL_CREATE:
					$this->actionIntervalCreate();
					break;
				case self::ACTION_INTERVAL_CLEAR_ALL:
					$this->actionIntervalClearAll();
					break;
				case self::ACTION_INTERVAL_DELETE:
					$this->actionIntervalDelete();
					break;
				default:
					throw new NotFoundException("Call undefined method {$this->getMethod()}.");
			}
		} catch (NotFoundException $e) {
			$this->printJsonResponse(['success' => false, 'message' => $e->getMessage()], 404);
		} catch (Throwable $e) {
			$this->printJsonResponse(['success' => false, 'message' => $e->getMessage()], 500);
		}
	}

	public function getMethod(): string
	{
		return $this->method;
	}

	/**
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public function actionIntervalCreate(): void
	{
		if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
			throw new NotFoundException("Wrong request method for action {$this->getMethod()}.");
		}

		$request = new Request($_POST);
		try {
			$request->validate();
		} catch (UserException $e) {
			$this->printJsonResponse(['success' => false, 'message' => $e->getMessage()]);
		}

		$intervals = $this->getDb()->getIntervalsByFilter($request->getRequest()['date_start'], $request->getRequest()['date_end']);

		$this->getIntervalManager()->createInterval(
			$intervals,
			DateTime::createFromFormat('Y-m-d', $request->getRequest()['date_start']),
			DateTime::createFromFormat('Y-m-d', $request->getRequest()['date_end']),
			(string)$request->getRequest()['price']
		);

		$this->printJsonResponse(['success' => true]);
	}

	/**
	 * @throws NotFoundException
	 * @throws Exception
	 */
	public function actionIntervalDelete(): void
	{
		if ($_SERVER['REQUEST_METHOD'] !== 'DELETE') {
			throw new NotFoundException("Wrong request method for action {$this->getMethod()}.");
		}

		$id = (int)$_GET['id'];

		$interval = $this->getDb()->getIntervalById($id);
		if (!$interval) {
			throw new NotFoundException("Interval by id {$id} not found.");
		}

		$this->getDb()->remove($id);

		$this->printJsonResponse(['success' => true]);
	}

	/**
	 * @throws Exception
	 */
	public function actionIntervalClearAll(): void
	{
		if ($_SERVER['REQUEST_METHOD'] !== 'DELETE') {
			throw new NotFoundException("Wrong request method for action {$this->getMethod()}.");
		}

		$this->getDb()->removeAll();

		$this->printJsonResponse(['success' => true]);
	}

	private function printJsonResponse(array $params = [], int $statusCode = 200): void
	{
		header('HTTP/1.0 ' . $statusCode . ' ' . $this->statusTextByCode[ $statusCode ]);
		header('Content-Type: application/json; charset=UTF-8');

		echo json_encode($params);
	}
}