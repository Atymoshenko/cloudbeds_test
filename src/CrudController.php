<?php

class CrudController {
	private const ACTION_INDEX = 'index';
	private const ACTION_API = 'api';

	private const ACTION_DEFAULT = 'index';

	/** @var Db */
	private $db;
	/** @var string */
	private $action;

	/**
	 * CrudController constructor.
	 *
	 * @param Db $db
	 *
	 * @throws Exception
	 */
	public function __construct(Db $db)
	{
		$this->db = $db;
		$this->action = $_GET['action'] ?? self::ACTION_DEFAULT;

		switch ($this->action) {
			case self::ACTION_INDEX:
				$this->indexAction();
				break;
			case self::ACTION_API:
				$this->apiAction();
				break;
			default:
				throw new Exception("Call undefined action {$this->action}.");
		}
	}

	/**
	 * @throws Exception
	 */
	public function indexAction(): void
	{
		$intervalList = $this->getDb()->getIntervals();

		$this->printResponse(['intervalList' => $intervalList]);
	}

	/**
	 * @throws Exception
	 */
	public function apiAction(): void
	{
		(new Api($this->getDb()))->run();
	}

	private function getDb(): Db
	{
		return $this->db;
	}

	private function getAction(): string
	{
		return $this->action;
	}

	private function printResponse(array $params = []): void
	{
		$params['actionView'] = __DIR__ . '/View/action' . ucfirst($this->getAction()) . '.html.php';
		extract($params);
		require __DIR__ . '/View/layout.html.php';
	}
}